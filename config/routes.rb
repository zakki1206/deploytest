Rails.application.routes.draw do
  resources :books
  get 'exec_java/index', to: 'exec_java#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
